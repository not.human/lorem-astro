from collections import Counter

def output_stats(ch, s, sc):

	#choisir quand c'est régulier ou pluriel

	f=open('stats.txt', 'w')
	f.write('%s\n\n' % ch)
	n=compter_lignes(s)
	f.write('%s ligne(s).\n' % n)
	n = compter_caracteres(s)
	f.write('%s charactère(s).\n' % n)
	n = compter_mots(s)
	f.write('%s mot(s).\n' % n)
	n = compter_souschaine(s, sc)
	f.write('%s "%s".\n' % (n, sc))
	n = compte_occurences_mots(s)
	f.write('%s mots different' % n)
	f.close()

def extraire_chaine(ch, enc):
	f = open(ch, 'r', encoding=enc)
	s = f.read()
	f.close()
	return s

def compter_lignes(s):
	return len(s.split('\n'))

def compter_caracteres(s):
	return len(s)

def compter_souschaine(s, sc):
	return s.count(sc)

def compter_mots(s):
	return len(s.split())

def compte_occurences_mots(s):
	listeDeMots = s.split()
	dictDeMots = dict(Counter(listeDeMots))
	return len(dictDeMots)

def most_common_word(s, n):
	listeDeMots = s.split()
	dictDeMots = dict(Counter(listeDeMots).most_common(n))
	return dictDeMots

def most_common_word_with_len(d):
	dictDeMotsWithLen = {}
	for value in d.values():
		dictDeMotsWithLen[value].append(len(value))

	print(dictDeMotsWithLen)


def longueurDesMots(s):
	listDeLongueurDeMots = []
	listeDeMots = s.split()
	for m in listeDeMots:
		listDeLongueurDeMots.append(len(m))