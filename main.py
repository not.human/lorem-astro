import sys
import io
from collections import Counter
import util


def main():
	ch = sys.argv[1]
	enc = sys.argv[2]
	sc = "Lorem"

	s = util.extraire_chaine(ch, enc)
	output_statsToDefined = util.output_stats(ch, s, sc)
	util.compte_occurences_mots(s)
	print(util.most_common_word(s, 200))
	util.longueurDesMots(s)
	util.most_common_word_with_len(util.most_common_word(s, 10))

	return 0

if __name__ == '__main__':
	main()

